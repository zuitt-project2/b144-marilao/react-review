import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import Courses from './pages/Courses';
import CourseView from './pages/CourseView';
import Error from './pages/Error';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import './App.css';
import { UserProvider } from './UserContext';


function App() {

	// State hook for the user state that's defined here for a global scope
	// Initialized as an object with properties from the localStorage
	// This will be used to store the user information and will be used for validating if a user is logged in on the app or not
    const [user, setUser] = useState({
		id: null,
		isAdmin: null
	});

    // Function for clearing localStorage on logout
    const unsetUser = () => {

		localStorage.clear();

		setUser({
			id: null,
			isAdmin: null
		});

    };

    //Because our user state's values are reset to null every time the user reloads the page (thus logging the user out), we want to use React's useEffect hook to fetch the logged-in user's details when the page is reloaded. By using the token saved in localStorage when a user logs in, we can fetch the their data from the database, and re-set the user state values back to the user's details.
    useEffect(() => {

		// console.log(user);

		fetch(`${ process.env.REACT_APP_API_URL }/users/details`, {
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
		})
		.then(res => res.json())
		.then(data => {

			// Set the user states values with the user details upon successful login.
			if (typeof data._id !== "undefined") {

				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				});

			// Else set the user states to the initial values
			} else {

				setUser({
					id: null,
					isAdmin: null
				});

			}

		})

    }, []);

	return (
		<UserProvider value={{user, setUser, unsetUser}}>
			<Router>
				<AppNavbar />
					<Switch>
						<Route exact path="/" component={Home}/>
						<Route exact path="/courses" component={Courses}/>
						<Route exact path="/courses/:courseId" component={CourseView}/>
						<Route exact path="/login" component={Login}/>
						<Route exact path="/logout" component={Logout}/>
						<Route exact path="/register" component={Register}/>
						<Route component={Error} />
					</Switch>
			</Router>
		</UserProvider>
	);
}

export default App;