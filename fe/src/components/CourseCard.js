import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import React from 'react';

export default function CourseCard({courseProp}) {

    // Deconstruct the course properties into their own variables
    const {_id, name, description, price} = courseProp;

    return (
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>₱{price}</Card.Text>
                <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
            </Card.Body>
        </Card>
    )
}